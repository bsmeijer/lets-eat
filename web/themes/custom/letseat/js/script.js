(function ($, Drupal, drupalSettings) {
    'use strict';

    // Navigation styles.
    Drupal.behaviors.carousel = {
        attach: function (context) {
            $('.view-carousel .view-content').once().slick({
                arrows: false,
                dots: true
            });
        }
    };

    Drupal.behaviors.userMenu = {
        attach: function (context) {
            $('.user-menu-button').once().click(function () {
                if ($('body').hasClass('user-menu-open')) {
                    $('body').removeClass('user-menu-open');
                }
                else {
                    $('body').addClass('user-menu-open');
                }
            });
        }
    };

    Drupal.behaviors.mainMenu = {
        attach: function (context) {
            $('.main-menu-button').once().click(function () {
                if ($('body').hasClass('main-menu-open')) {
                    $('body').removeClass('main-menu-open');
                }
                else {
                    $('body').addClass('main-menu-open');
                }
            });
        }
    };

    Drupal.behaviors.searchBar = {
        attach: function (context) {
            $('.search-bar-button').once().click(function () {
                if ($('body').hasClass('search-bar-open')) {
                    $('body').removeClass('search-bar-open');
                }
                else {
                    $('body').addClass('search-bar-open');
                }
            });
        }
    };
})(jQuery, Drupal, drupalSettings);